package Year2017;
import org.testng.annotations.Test;

import java.util.*;

/**
 * Not proud of this but it completed its task.
 */
public class Day6 {

    private static final String INPUT = "14\t0\t15\t12\t11\t11\t3\t5\t1\t6\t8\t4\t9\t1\t8\t4";

    @Test(priority = 1)
    public void day5Tests() {
        execute(getIntList("0\t2\t7\t0"));
    }

    @Test(priority = 2, dependsOnMethods = "day5Tests")
    public void day5Actual() {
        execute(getIntList(INPUT));
    }

    private void execute(List<Integer> banks) {
        List<List<Integer>> seen = new ArrayList<>();
        seen.add(banks);
        int duplicateId;
        while(true) {
            List<Integer> tmp = new ArrayList<>(seen.get(seen.size() - 1));
            int max = Collections.max(tmp);
            int idx = tmp.indexOf(max);
            int spread = max < tmp.size() ? 1 : max / (tmp.size() - 1);

            int count = 0;
            tmp.set(idx, 0);
            while (max > 0) {
                int update = (count + idx + 1) % tmp.size();
                if (update == idx) {
                    tmp.set(update, max);
                } else {
                    int curr = tmp.get(update);
                    tmp.set(update, (curr + spread));
                }
                count++;
                max -= spread;
            }
            if (seen.contains(tmp)) {
                duplicateId = seen.indexOf(tmp) + 1;
                seen.add(tmp);
                break;
            }
            seen.add(tmp);
        }
        System.out.println("seen: " + seen);
        System.out.println("cycles til seen before: " + (seen.size() - 1));
        System.out.println("cycles between seen before: " + (seen.size() - duplicateId));
    }

    private List<Integer> getIntList(String input) {
        String[] split = input.split("\t");
        List<Integer> banks = new ArrayList<>();
        for (String s : split) {
            banks.add(Integer.parseInt(s));
        }
        return banks;
    }
}