import re
import sys

with open('Day3_1_input.txt') as f:
  content = f.readlines()
content = [x.strip() for x in content] 

def Day3_1(the_content, matrix_size, print_matrix):
  Matrix = [["." for x in range(matrix_size)] for y in range(matrix_size)]
  for row in the_content:
    matchObj = re.match(r'#([0-9]*) @ ([0-9]*),([0-9]*): ([0-9]*)x([0-9]*)', row, re.M|re.I)
    if matchObj:
      id = int(matchObj.group(1))
      startX = int(matchObj.group(2))
      startY = int(matchObj.group(3))
      w = int(matchObj.group(4))
      h = int(matchObj.group(5))
      
      for x in range(startX, startX + w):
        for y in range(startY, startY + h):
          if Matrix[x][y] is not ".":
            Matrix[x][y] = "x"
          else:
            Matrix[x][y] = str(id)

  if print_matrix:
    print_matrix(Matrix)
  print ("Number of square inches within two or more claims: ", find_xes(Matrix))

def find_xes(matrix):
  xes = 0
  for row in matrix:
    for c in row:
      if c == "x":
        xes += 1
  return xes
  
def print_matrix(matrix):
  print ("Matrix")
  for row in matrix:
    for c in row:
      sys.stdout.write(c)
    print("")

if __name__ == '__main__':
  """
  Day3_1(['abcdef'])
  Day3_1(['bababc'])
  Day3_1(['abbcde'])
  Day3_1(['abcccd'])
  Day3_1(['aabcdd'])
  Day3_1(['abcdee'])
  Day3_1(['ababab'])
  Day3_1(['#1 @ 1,3: 4x4', '#2 @ 3,1: 4x4', '#3 @ 5,5: 2x2'], 15)
  Day3_1(['#1 @ 1,3: 4x4'])
  """
  Day3_1(content, 2000, False)