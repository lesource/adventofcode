with open('Day1_1_input.txt') as f:
  content = f.readlines()
content = [x.strip() for x in content] 

def Day1_1(the_content):
  num = 0
  for val in the_content:
    num += int(val)
  print("Day1_1: Final number: ", num)
  
def Day1_2(the_content):
  idx = 0
  num = 0
  seen_set = {0}
  while True:
    num += int(the_content[idx])
    if num in seen_set:
      print("Day1_2: First value seen twice is: ", num)
      break
    seen_set.add(num)
    if idx == (len(the_content) - 1):
      idx = 0
    else:
      idx += 1
    
if __name__ == '__main__':
  Day1_1(content)
  Day1_2(content)